#!/usr/bin/bash
set -e

# Exit if no user and host provided.
if [ "$#" -ne 1 ]; then
        echo "Usage: $0 <app>"
        exit 1
fi
#Set variables
# Get user and host from input.
APP="$1"

#get all the PID related to the running application name
pwd
AppPID=$(pgrep -f ${APP})
echo $AppPID > app.pid
cat app.pid
