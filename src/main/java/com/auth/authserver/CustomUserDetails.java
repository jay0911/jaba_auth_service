package com.auth.authserver;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.auth.authserver.dto.LoginResponse;
import com.auth.authserver.dto.Response;
import com.auth.authserver.properties.LoginApiProperties;
import com.auth.authserver.util.HashUtil;

@Component("userDetailsService")
public class CustomUserDetails implements UserDetailsService {
	
	@Autowired
	LoginApiProperties jabaApiProperties;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ADMIN"));
		
		RestTemplate rt = new RestTemplate();
	    ResponseEntity<Response<LoginResponse>> resp = rt.exchange(jabaApiProperties.getBaseurl().concat(jabaApiProperties.getLogin().concat(username)), HttpMethod.GET, null,
	    		new ParameterizedTypeReference<Response<LoginResponse>>() {});
		
	    String password = "";
	    
		try {
			password = HashUtil.decrypt(resp.getBody().getData().getPassword());		
		} catch (Exception e) {
			throw new UsernameNotFoundException(e.getMessage());
		}	
		return new User(resp.getBody().getData().getUsername(), password, authorities);	

	}
}
