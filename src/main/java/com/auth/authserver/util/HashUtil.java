package com.auth.authserver.util;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class HashUtil {

    static Cipher cipher;  
    
    static SecretKey secretKey;
    
    public static void writeSecretKey() throws NoSuchAlgorithmException, IOException {
        /* 
        create key 
        If we need to generate a new key use a KeyGenerator
        If we have existing plaintext key use a SecretKeyFactory
       */ 
       KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
       keyGenerator.init(128); // block size is 128bits
       SecretKey secretKey = keyGenerator.generateKey();
       
       
       String current = new File( "." ).getCanonicalPath();
       FileOutputStream fos = new FileOutputStream(current+"\\secretkey.ser");
       ObjectOutputStream oos = new ObjectOutputStream(fos);
	   oos.writeObject(secretKey);
	   oos.close();
    }
    
    public static void readSecretKey() throws ClassNotFoundException, IOException {
		ObjectInputStream ois = new ObjectInputStream(HashUtil.class.getResourceAsStream("/secretkey.ser"));
		secretKey = (SecretKey) ois.readObject();
		ois.close();
    }

    public static String encrypt(String plainText)
            throws Exception {
    	readSecretKey();
        cipher = Cipher.getInstance("AES"); //SunJCE provider AES algorithm, mode(optional) and padding schema(optional)  
        byte[] plainTextByte = plainText.getBytes();
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptedByte = cipher.doFinal(plainTextByte);
        Base64.Encoder encoder = Base64.getEncoder();
        String encryptedText = encoder.encodeToString(encryptedByte);
        return encryptedText;
    }

    public static String decrypt(String encryptedText)
            throws Exception {
    	readSecretKey();
        cipher = Cipher.getInstance("AES"); //SunJCE provider AES algorithm, mode(optional) and padding schema(optional)  
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] encryptedTextByte = decoder.decode(encryptedText);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
        String decryptedText = new String(decryptedByte);
        return decryptedText;
    }
}
